/* Reduced Ordered Binary Decision Diagrams */

#pragma once

typedef struct BddNode* Bdd;

/* initialization */

void bdd_init (int num_vars);

/* base functions */

Bdd bdd_true ();

Bdd bdd_false ();

Bdd bdd_ithvar (int i);

/* function operators */

Bdd bdd_not (Bdd bdd);

Bdd bdd_and (Bdd lhs, Bdd rhs);

Bdd bdd_or (Bdd lhs, Bdd rhs);

Bdd bdd_xor (Bdd lhs, Bdd rhs);

Bdd bdd_implies (Bdd lhs, Bdd rhs);

Bdd bdd_equiv (Bdd lhs, Bdd rhs);

Bdd bdd_ite (Bdd I, Bdd T, Bdd E);

Bdd bdd_restrict (Bdd bdd, int var, int val);

Bdd bdd_exists (Bdd bdd, int var);

/* debugging */

void bdd_print (Bdd bdd);


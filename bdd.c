#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "math.h"
#include "bdd.h"

/* store size is 2^16, cache 2^14 */

#define NODESTORESIZE 65536
#define NODESTOREMASK 0xffffUL
#define ITECACHESIZE  16384
#define ITECACHEMASK  0x3fffUL

/* data structures */

struct BddNode
{
   int16_t  var;   /* variable at this node, set to #vars + 1 for terminal nodes */
   Bdd      hi;    /* true branch */
   Bdd      lo;    /* false branch */
   Bdd      next;  /* chain in nodestore hash table */
};

struct CachedIte
{
   Bdd      I;
   Bdd      T;
   Bdd      E;
   Bdd      res;   /* 0 for empty entries */
};

/* static data */

static struct BddNode s_true;                    /* constant true function */
static struct BddNode s_false;                   /* constant false function */
static Bdd NODESTORE [NODESTORESIZE];            /* unique node table, chained */
static struct CachedIte ITECACHE [ITECACHESIZE]; /* ite cache, unchained */

/* initialization */

void bdd_init (int num_vars)
{
   int i;
   Bdd bdd, next;

   s_true.var = num_vars + 1;             /* adjust true function var index */
   s_false.var = num_vars + 1;            /* adjust false function var index */
   for (i = 0; i < NODESTORESIZE; ++i) {  /* deallocate bdd nodes */
      for (bdd = NODESTORE[i]; bdd; ) {
         next = bdd->next;
         free(bdd);
         bdd = next;
      }
      NODESTORE[i] = 0;
   }   
   for (i = 0; i < ITECACHESIZE; ++i)     /* clear cache */
      ITECACHE[i].res = 0;
}

/* base functions */

Bdd bdd_true ()
{
   return &s_true;
}

Bdd bdd_false ()
{
   return &s_false;
}

static uint32_t hash_ptr (void* ptr)
{
   return (2654435761ULL * (uintptr_t)ptr) >> 16;
}

static uint32_t hash_node (int16_t i, Bdd hi, Bdd lo)
{
   uint32_t h1 = hash_ptr(hi), 
            h2 = hash_ptr(lo);
   return ((h1*3 + h2)*3 + i) & NODESTOREMASK;
}

static Bdd make_node (int16_t var, Bdd hi, Bdd lo)
{
   uint32_t h;
   Bdd bdd, bddnew;

   /* eliminate redundant nodes */
   if (hi == lo)
      return hi;
   /* return possible matching node, otherwise set bdd to last node */
   h = hash_node(var, lo, hi);
   bdd = NODESTORE[h];
   while (bdd) {
      if (bdd->var == var && bdd->hi == hi && bdd->lo == lo)
         return bdd;
      if (bdd->next)
         bdd = bdd->next;
      else
         break;
   }
   /* no match, create new node */
   bddnew = malloc(sizeof(struct BddNode));
   bddnew->var = var; bddnew->hi = hi; bddnew->lo = lo; bddnew->next = 0;
   if (!bdd) 
      NODESTORE[h] = bddnew;
   else 
      bdd->next = bddnew;
   return bddnew;
}

Bdd bdd_ithvar (int i)
{
   if (i < 1 || i >= s_true.var) return 0;
   return make_node(i, bdd_true(), bdd_false());
}

/* function operations */

Bdd bdd_restrict (Bdd bdd, int var, int val)
{
   if (bdd->var > var) 
      return bdd;
   if (bdd->var < var)
      return make_node(bdd->var, bdd_restrict(bdd->hi, var, val),
         bdd_restrict(bdd->lo, var, val));
   return val ? bdd->hi : bdd->lo;
}

Bdd bdd_exists (Bdd bdd, int var)
{
   return bdd_or(bdd_restrict(bdd, var, 0), bdd_restrict(bdd, var, 1));
}

static uint32_t hash_ite (Bdd I, Bdd T, Bdd E)
{
   uint32_t h1 = hash_ptr(I), 
            h2 = hash_ptr(T), 
            h3 = hash_ptr(E);
   return ((h1*3 + h2)*3 + h3) & ITECACHEMASK;
}

static Bdd bdd_ite_split (Bdd I, Bdd T, Bdd E)
{
   /* split on top-most variable */
   int16_t var = I->var;
   if (T->var < var) var = T->var;
   if (E->var < var) var = E->var;

   Bdd Ixt = bdd_restrict(I, var, 1);
   Bdd Txt = bdd_restrict(T, var, 1);
   Bdd Ext = bdd_restrict(E, var, 1);
   Bdd tbranch = bdd_ite(Ixt, Txt, Ext);

   Bdd Ixf = bdd_restrict(I, var, 0);
   Bdd Txf = bdd_restrict(T, var, 0);
   Bdd Exf = bdd_restrict(E, var, 0);
   Bdd fbranch = bdd_ite(Ixf, Txf, Exf);

   return make_node(var, tbranch, fbranch);
}

Bdd bdd_ite (Bdd I, Bdd T, Bdd E)
{
   /* base cases */
   if (I == bdd_true())  return T;
   if (I == bdd_false()) return E;
   if (T == E)           return T;
   if (T == bdd_true() && E == bdd_false()) return I;

   /* look up operation in cache */
   uint32_t h = hash_ite(I, T, E);
   struct CachedIte* c = &ITECACHE[h];
   if (c->res && c->I == I && c->T == T && c->E == E)
      return c->res;

   /* otherwise calculate operation recursively */
   Bdd res = bdd_ite_split(I, T, E);
   c->I = I; c->T = T; c->E = E; c->res = res;
   return res;
}

Bdd bdd_not (Bdd bdd)
{
   return bdd_ite (bdd, bdd_false(), bdd_true());
}

Bdd bdd_and (Bdd lhs, Bdd rhs)
{
   return bdd_ite (lhs, rhs, bdd_false());
}

Bdd bdd_or (Bdd lhs, Bdd rhs)
{
   return bdd_ite (lhs, bdd_true(), rhs);
}

Bdd bdd_xor (Bdd lhs, Bdd rhs)
{
   return bdd_ite (lhs, bdd_not(rhs), rhs);
}

Bdd bdd_implies (Bdd lhs, Bdd rhs)
{
   return bdd_ite (lhs, rhs, bdd_true());
}

Bdd bdd_equiv (Bdd lhs, Bdd rhs)
{
   return bdd_ite (lhs, rhs, bdd_not(rhs));
}

/* debugging */

void bdd_print (Bdd bdd)
{
   int i;

   for (i = 0; i < (bdd->var - 1) * 2; ++i) printf(" ");
   if (bdd == bdd_true() ) printf("[1]\n");
   else if (bdd == bdd_false()) printf("[0]\n");
   else printf("(x%d) ->\n", bdd->var);
   if (bdd->hi) 
      bdd_print(bdd->hi);
   if (bdd->lo) {
      for (i = 0; i < (bdd->var - 1) * 2; ++i) printf(" ");
      printf("-----\n");
      bdd_print(bdd->lo);
   }
}

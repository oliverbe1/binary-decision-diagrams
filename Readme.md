Binary Decision Diagrams
========================

This is a very minimal implementation of binary decision diagrams (BDDs) in C 
that I used to write a model checker. A binary decision diagram is a 
compact representation of a boolean function. 

One important use for BDDs is representing subsets and relations 
between finite sets. If you assign a bitstring to each element in a set,
then for every subset you can construct a boolean function which will evaluate 
to 1 for exactly those bitstrings assigned to members of the subset.

## Example Usage ##

Following program tests some well-known laws in propositional logic. 
Before calling any function, the bdd_init(n) needs to be called with 
the number of arguments of your boolean functions.
See `bdd.h` for the full API.

```C
#include "assert.h"
#include "bdd.h"

int main ()
{
   bdd_init(3);

   Bdd p = bdd_ithvar(1);
   Bdd q = bdd_ithvar(2);
   Bdd r = bdd_ithvar(3);

   /* !(p ^ q) <-> !p v !q */
   Bdd morgan = bdd_equiv(bdd_not(bdd_and(p, q)),
                          bdd_or(bdd_not(p), bdd_not(q)));

   /* p ^ (q v r) <-> (p ^ q) v (p ^ r) */
   Bdd distrib = bdd_equiv(bdd_and(p, bdd_or(q, r)),
                           bdd_or(bdd_and(p, q), bdd_and(p, r)));

   /* p v !p */
   Bdd lem = bdd_or(p, bdd_not(p));

   /* p ^ !p */
   Bdd contradiction = bdd_and(p, bdd_not(p));

   /* (p ^ q) v !p */
   Bdd satisfiable = bdd_or(bdd_and(p, q), bdd_not(p));

   assert( morgan        == bdd_true()  );
   assert( distrib       == bdd_true()  );
   assert( lem           == bdd_true()  );
   assert( contradiction == bdd_false() );
   assert( satisfiable   != bdd_false() );

   return 0;
}
```

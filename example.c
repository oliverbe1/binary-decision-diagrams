/*
 * Small example program that verifies some well known laws 
 * in propositional logic.
 */
#include "assert.h"
#include "bdd.h"

int main ()
{
   bdd_init(3);

   Bdd p = bdd_ithvar(1);
   Bdd q = bdd_ithvar(2);
   Bdd r = bdd_ithvar(3);

   /* !(p ^ q) <-> !p v !q */
   Bdd morgan = bdd_equiv(bdd_not(bdd_and(p, q)),
                          bdd_or(bdd_not(p), bdd_not(q)));

   /* p ^ (q v r) <-> (p ^ q) v (p ^ r) */
   Bdd distrib = bdd_equiv(bdd_and(p, bdd_or(q, r)),
                           bdd_or(bdd_and(p, q), bdd_and(p, r)));

   /* p v !p */
   Bdd lem = bdd_or(p, bdd_not(p));

   /* p ^ !p */
   Bdd contradiction = bdd_and(p, bdd_not(p));

   /* (p ^ q) v !p */
   Bdd satisfiable = bdd_or(bdd_and(p, q), bdd_not(p));

   assert( morgan        == bdd_true()  );
   assert( distrib       == bdd_true()  );
   assert( lem           == bdd_true()  );
   assert( contradiction == bdd_false() );
   assert( satisfiable   != bdd_false() );

   return 0;
}
